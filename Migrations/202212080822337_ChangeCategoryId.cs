﻿namespace HiMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCategoryId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Restaurants", new[] { "CategoryId" });
            AlterColumn("dbo.Restaurants", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Restaurants", "CategoryId");
            AddForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Restaurants", new[] { "CategoryId" });
            AlterColumn("dbo.Restaurants", "CategoryId", c => c.Int());
            CreateIndex("dbo.Restaurants", "CategoryId");
            AddForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories", "Id");
        }
    }
}
