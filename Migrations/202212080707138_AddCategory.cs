﻿namespace HiMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Restaurants", "CategoryId", c => c.Int());
            CreateIndex("dbo.Restaurants", "CategoryId");
            AddForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Restaurants", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Restaurants", new[] { "CategoryId" });
            DropColumn("dbo.Restaurants", "CategoryId");
            DropTable("dbo.Categories");
        }
    }
}
