﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace HiMVC.Models
{
    /// <summary>
    /// 食堂基本資訊
    /// </summary>
    public class Restaurant
    {
        [DisplayName("唉低")]
        public int Id { get; set; }

        [DisplayName("名稱")]
        [Required]
        public string Name { get; set; }

        [DisplayName("地址")]
        public string Address { get; set; }

        [DisplayName("電話")]
        public string PhoneNumber { get; set; }

        [DisplayName("星數")]
        [Range(0, 5)]
        public double? Stars { get; set; }

        [DisplayName("評價數")]
        [Range(0, double.PositiveInfinity)]
        public int? Comments { get; set; }

        [DisplayName("標籤")]
        public string Tags { get; set; }

        [DisplayName("類別")]
        [Required]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        [NotMapped]
        public List<string> TagsList
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Tags))
                {
                    return new List<string>();
                }
                return Tags?.Split(',').ToList();
            }
            set
            {
                if (value?.Count > 0)
                {
                    Tags = string.Join(",", value) + ",";
                }
                else
                {
                    Tags = string.Empty;
                }
            }
        }
    }
}