﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HiMVC.Models
{
    /// <summary>
    /// 評價
    /// </summary>
    public class Review
    {
        [DisplayName("唉低")]
        public int Id { get; set; }

        [DisplayName("小夥伴")]
        [Required]
        public int FellowId { get; set; }

        [DisplayName("食堂")]
        [Required]
        public int RestaurantId { get; set; }

        [DisplayName("星數")]
        [Range(0, 5)]
        public double? Stars { get; set; }

        [DisplayName("評價")]

        public string Comment { get; set; }

        [DisplayName("小夥伴")]
        public virtual Fellow Fellow { get; set; }

        [DisplayName("食堂")]
        public virtual Restaurant Restaurant { get; set; }
    }
}