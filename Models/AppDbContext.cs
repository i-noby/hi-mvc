﻿using System.Data.Entity;

namespace HiMVC.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("Data Source=localhost\\SQLEXPRESS;Initial Catalog=Hi;Integrated Security=True") { }

        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Fellow> Fellows { get; set; }
        public DbSet<Review> Reviews { get; set; }
    }
}