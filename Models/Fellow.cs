﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HiMVC.Models
{
    /// <summary>
    /// 小夥伴基本資訊
    /// </summary>
    public class Fellow
    {
        [DisplayName("唉低")]
        public int Id { get; set; }

        [DisplayName("姓名")]
        [Required]
        public string Name { get; set; }
    }
}