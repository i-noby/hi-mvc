﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HiMVC.Models
{
    public class Category
    {
        [DisplayName("唉低")]
        public int Id { get; set; }

        [DisplayName("名稱")]
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Restaurant> Restaurants { get; set; }
    }
}