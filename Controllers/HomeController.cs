﻿using System.Web.Mvc;

namespace HiMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        [ActionName("Profile")]
        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}