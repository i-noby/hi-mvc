﻿using HiMVC.Models;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HiMVC.Controllers
{
    public class FellowsController : Controller
    {
        private readonly AppDbContext db = new AppDbContext();

        /// <summary>
        /// [GET] 小夥伴列表
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            return View(await db.Fellows.ToListAsync());
        }

        /// <summary>
        /// [GET] 小夥伴詳情
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fellow = await db.Fellows.FindAsync(id);
            if (fellow == null)
            {
                return HttpNotFound();
            }
            return View(fellow);
        }

        /// <summary>
        /// [GET] 新增小夥伴
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// [POST] 新增小夥伴
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] Fellow fellow)
        {
            if (ModelState.IsValid)
            {
                db.Fellows.Add(fellow);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(fellow);
        }

        /// <summary>
        /// [GET] 修改小夥伴
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fellow = await db.Fellows.FindAsync(id);
            if (fellow == null)
            {
                return HttpNotFound();
            }
            return View(fellow);
        }

        /// <summary>
        /// [POST] 修改小夥伴
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] Fellow fellow)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fellow).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(fellow);
        }

        /// <summary>
        /// [GET] 刪除小夥伴
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fellow = await db.Fellows.FindAsync(id);
            if (fellow == null)
            {
                return HttpNotFound();
            }
            return View(fellow);
        }

        /// <summary>
        /// [POST] 刪除小夥伴
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var fellow = await db.Fellows.FindAsync(id);
            if (fellow == null)
            {
                return HttpNotFound();
            }
            db.Fellows.Remove(fellow);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
