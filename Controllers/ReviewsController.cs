﻿using HiMVC.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HiMVC.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly AppDbContext db = new AppDbContext();

        public async Task<ActionResult> Index(int? id)
        {
            var reviews = db.Reviews.Include(r => r.Fellow).Include(r => r.Restaurant);
            if (id != null)
            {
                reviews = reviews.Where(x => x.RestaurantId == id);
            }
            return View(await reviews.ToListAsync());
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var review = await db.Reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        public ActionResult Create()
        {
            InitDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FellowId,RestaurantId,Stars,Comment")] Review review)
        {
            if (ModelState.IsValid)
            {
                var hasReviewed = await db.Reviews
                    .Where(x => x.RestaurantId == review.RestaurantId)
                    .Where(x => x.FellowId == review.FellowId)
                    .CountAsync() > 0;
                ViewBag.HasReviewed = hasReviewed;
                if (!hasReviewed)
                {
                    var restaurant = await db.Restaurants.FindAsync(review.RestaurantId);
                    if (restaurant == null)
                    {
                        return HttpNotFound();
                    }
                    restaurant.Stars = db.Reviews.Where(x => x.Stars != null).Average(x => x.Stars);
                    restaurant.Stars = Math.Round(restaurant.Stars.Value, 1);
                    restaurant.Comments = db.Reviews.Where(x => x.Comment != null).Count();
                    db.Reviews.Add(review);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            InitDropDownList(review);
            return View(review);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var review = await db.Reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            InitDropDownList(review);
            return View(review);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FellowId,RestaurantId,Stars,Comment")] Review review)
        {
            if (ModelState.IsValid)
            {
                db.Entry(review).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            InitDropDownList(review);
            return View(review);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var review = await db.Reviews.FindAsync(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var review = await db.Reviews.FindAsync(id);
            db.Reviews.Remove(review);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitDropDownList(Review review = null)
        {
            ViewBag.FellowId = new SelectList(db.Fellows, "Id", "Name", review?.FellowId);
            ViewBag.RestaurantId = new SelectList(db.Restaurants, "Id", "Name", review?.RestaurantId);
        }
    }
}
