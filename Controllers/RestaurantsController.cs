﻿using HiMVC.Models;
using HiMVC.ViewModels;
using LinqKit;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HiMVC.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly AppDbContext db = new AppDbContext();

        /// <summary>
        /// [GET] 食堂列表
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index(RestaurantViewModel restaurantViewModel)
        {
            var query = db.Restaurants.Include(x => x.Category).AsQueryable();
            if (!string.IsNullOrWhiteSpace(restaurantViewModel.Name))
            {
                query = query.Where(x => x.Name.Contains(restaurantViewModel.Name));
            }
            if (!string.IsNullOrWhiteSpace(restaurantViewModel.Address))
            {
                query = query.Where(x => x.Address.Contains(restaurantViewModel.Address));
            }
            if (!string.IsNullOrWhiteSpace(restaurantViewModel.PhoneNumber))
            {
                query = query.Where(x => x.PhoneNumber.Contains(restaurantViewModel.PhoneNumber));
            }
            if (restaurantViewModel.Stars?.Start > 0)
            {
                query = query.Where(x => x.Stars >= restaurantViewModel.Stars.Start);
            }
            if (restaurantViewModel.Stars?.End > 0)
            {
                query = query.Where(x => x.Stars <= restaurantViewModel.Stars.End);
            }
            if (restaurantViewModel.Comments?.Start > 0)
            {
                query = query.Where(x => x.Comments >= restaurantViewModel.Comments.Start);
            }
            if (restaurantViewModel.Comments?.End > 0)
            {
                query = query.Where(x => x.Comments <= restaurantViewModel.Comments.End);
            }
            if (!string.IsNullOrWhiteSpace(restaurantViewModel.Tags))
            {
                var tagQuery = PredicateBuilder.New<Restaurant>(false);
                var tags = restaurantViewModel.Tags.Split(',');
                foreach (var tag in tags)
                {
                    tagQuery = tagQuery.Or(x => x.Tags.Contains(tag + ","));
                }
                query = query.Where(tagQuery);
            }
            restaurantViewModel.Restaurants = await query
                .OrderByDescending(x => x.Id)
                .Skip((restaurantViewModel.Page.Value - 1) * restaurantViewModel.Size.Value)
                .Take(restaurantViewModel.Size.Value)
                .ToListAsync();
            var total = await query.CountAsync();
            restaurantViewModel.Total = total;
            return View(restaurantViewModel);
        }

        /// <summary>
        /// [GET] 食堂詳情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var restaurant = await db.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            return View(restaurant);
        }

        /// <summary>
        /// [GET] 新增食堂
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            return View();
        }

        /// <summary>
        /// [POST] 新增食堂
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Name,Address,PhoneNumber,Tags,CategoryId")] Restaurant restaurant
        )
        {
            if (ModelState.IsValid)
            {
                db.Restaurants.Add(restaurant);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", restaurant.CategoryId);
            return View(restaurant);
        }

        /// <summary>
        /// [GET] 修改食堂
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var restaurant = await db.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", restaurant.CategoryId);
            return View(restaurant);
        }

        /// <summary>
        /// [POST] 修改食堂
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Exclude = "Stars,Comments")] Restaurant restaurant
        )
        {
            if (ModelState.IsValid)
            {
                var target = await db.Restaurants.FindAsync(restaurant.Id);
                if (restaurant == null)
                {
                    return HttpNotFound();
                }
                target.Name = restaurant.Name;
                target.Address = restaurant.Address;
                target.PhoneNumber = restaurant.PhoneNumber;
                target.Tags = restaurant.Tags;
                target.CategoryId = restaurant.CategoryId;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", restaurant.CategoryId);
            return View(restaurant);
        }

        /// <summary>
        /// [GET] 刪除食堂
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var restaurant = await db.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            return View(restaurant);
        }

        /// <summary>
        /// [POST] 刪除食堂
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await db.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            db.Restaurants.Remove(restaurant);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// [GET] 評價食堂
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Review(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var restaurant = await db.Restaurants.FindAsync(id);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            var reviewViewModel = new ReviewViewModel { Restaurant = restaurant };
            return View(reviewViewModel);
        }

        /// <summary>
        /// [POST] 評價食堂
        /// </summary>
        /// <param name="reviewViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Review(ReviewViewModel reviewViewModel)
        {
            var restaurant = await db.Restaurants.FindAsync(reviewViewModel.Review.RestaurantId);
            if (restaurant == null)
            {
                return HttpNotFound();
            }
            var fellow = await db.Fellows.FirstOrDefaultAsync(
                x => x.Name == reviewViewModel.Review.Fellow.Name
            );
            if (fellow == null)
            {
                fellow = db.Fellows.Add(reviewViewModel.Review.Fellow);
                await db.SaveChangesAsync();
            }
            var hasReviewed =
                await db.Reviews
                    .Where(x => x.RestaurantId == reviewViewModel.Review.RestaurantId)
                    .Where(x => x.FellowId == fellow.Id)
                    .CountAsync() > 0;
            ViewBag.HasReviewed = hasReviewed;
            if (!hasReviewed)
            {
                reviewViewModel.Review.Fellow = fellow;
                db.Reviews.Add(reviewViewModel.Review);
                await db.SaveChangesAsync();
                restaurant.Stars = await db.Reviews
                    .Where(x => x.RestaurantId == restaurant.Id)
                    .Where(x => x.Stars != null)
                    .AverageAsync(x => x.Stars);
                restaurant.Stars = Math.Round(restaurant.Stars.Value, 1);
                restaurant.Comments = await db.Reviews
                    .Where(x => x.RestaurantId == restaurant.Id)
                    .Where(x => x.Comment != null)
                    .CountAsync();
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            reviewViewModel.Restaurant = restaurant;
            return View(reviewViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
