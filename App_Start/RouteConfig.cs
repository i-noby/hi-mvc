﻿using HiMVC.Extensions;
using System.Web.Mvc;
using System.Web.Routing;

namespace HiMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                },
                namespaces: new string[] { "HiMVC.Controllers" }
            ).SetRouteName("Default");

            routes.MapRoute(
                name: "Pages",
                url: "{controller}/{type}/{id}/{title}",
                defaults: new
                {
                    controller = "Pages",
                    action = "Index",
                },
                constraints: new
                {
                    controller = "Pages",
                    action = "Index",
                }
            ).SetRouteName("Pages");
        }
    }
}
