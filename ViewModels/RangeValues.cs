﻿namespace HiMVC.ViewModels
{
    public class RangeValues<T>
    {
        public T Start { get; set; }

        public T End { get; set; }
    }
}
