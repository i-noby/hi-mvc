﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HiMVC.ViewModels
{
    public abstract class Pagination
    {
        public int? Page { get; set; } = 1;
        [Range(1, 200)]
        public int? Size { get; set; } = 10;
        public int? Total { get; set; }
        public int? LastPage
        {
            get
            {
                if (!Size.HasValue || !Total.HasValue)
                {
                    return 1;
                }
                return Total.Value / Size.Value + (Total.Value % Size.Value > 0 ? 1 : 0);
            }
        }
        public bool IsFirst { get { return Page == 1; } }
        public bool IsLast { get { return Page == LastPage; } }
        public IEnumerable<PageItem> GetPageItems()
        {
            var pageItems = new List<PageItem>();
            for (var i = 1; i <= LastPage; i++)
            {
                var pageItem = new PageItem
                {
                    Page = i,
                    IsCurrent = i == Page,
                    IsFirst = i == 1,
                    IsLast = i == LastPage,
                };
                pageItems.Add(pageItem);
            }
            return pageItems;
        }
    }

    public class PageItem
    {
        public int Page { get; set; }
        public bool IsCurrent { get; set; }
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
    }
}

