﻿using HiMVC.Models;

namespace HiMVC.ViewModels
{
    public class ReviewViewModel
    {
        public Restaurant Restaurant { get; set; }

        public Review Review { get; set; }
    }
}