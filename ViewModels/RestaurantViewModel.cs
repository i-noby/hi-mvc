﻿using HiMVC.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace HiMVC.ViewModels
{
    public class RestaurantQueryModel : Pagination
    {
        [DisplayName("名稱")]
        public string Name { get; set; }

        [DisplayName("類別")]
        public string CategoryName { get; set; }

        [DisplayName("地址")]
        public string Address { get; set; }

        [DisplayName("電話")]
        public string PhoneNumber { get; set; }

        [DisplayName("星數")]
        public RangeValues<float?> Stars { get; set; }

        [DisplayName("評價數")]
        public RangeValues<int?> Comments { get; set; }

        [DisplayName("標籤")]
        public string Tags { get; set; }
    }

    public class RestaurantViewModel : RestaurantQueryModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
    }
}