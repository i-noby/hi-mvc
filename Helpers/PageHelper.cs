﻿using System.Web;

namespace HiMVC.Helpers
{
    public static class PageHelper
    {
        public static string GetPagingUrl(HttpRequestBase req, int page)
        {
            var url = req.Url.ToString();
            if (req.Params["Page"] == null)
            {
                var prefix = url.IndexOf("?") == -1 ? "?" : "&";
                url += $"{prefix}Page={page}";
            }
            else
            {
                var orgPage = $"Page={req.Params["Page"]}";
                var newPage = $"Page={page}";
                url = url.Replace(orgPage, newPage);
            }
            return url;
        }
    }
}